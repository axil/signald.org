---
title: IRC
---

[#signald on freenode](irc://irc.freenode.net/#signald) can be used to discuss signald in real time.

[**Click here to connect in a browser**](https://kiwiirc.com/nextclient/irc.freenode.net/#signald)

If you prefer to use matrix, it seems that `#signald:matrix.org` has been set up as a bridge.