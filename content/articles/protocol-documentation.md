---
title: Protocol Documentation
---

The signald client protocol can document itself. The actual data structure of the documentation is not
documented at this time.

There are two ways to get the protocol documentation from signald:
* via the command line: `signald --dump-protocol > protocol.json`
* via a client socket: `{"type": "protocol", "version": "v1"}`

Additionally, the latest version of the protocol document can be downloaded from [here](/protocol.json)

# Examples

some things that are known to use the protocol documentation:

* Parts of this site are generated. See [generate.go](https://gitlab.com/signald/signald.org/-/blob/main/generate.go),
    which creates the markdown files for the protocol data. The final result can be found in the sidebar, under "Protocol"
* signald-go is mostly generated. See [tools/generator/main.go](https://gitlab.com/signald/signald-go/-/blob/main/tools/generator/main.go).
* pysignald_async is in large part generated. See [util/generate_api.py](https://gitlab.com/nicocool84/pysignald-async/-/blob/master/util/generate_api.py).
