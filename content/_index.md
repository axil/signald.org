---
title: signald
---
*unofficial daemon for interacting with Signal*

{{<hint info>}}
This site is a work in progress. If you have questions, [file an issue](https://gitlab.com/signald/signald/-/issues/new) or ask on [IRC](/articles/IRC/).
{{</hint>}}

[![source](/badges/stars.svg)](https://gitlab.com/signald/signald)
[![docker](/badges/docker.svg)](https://hub.docker.com/r/finn/signald)
[![GPL](/badges/license.svg)](https://gitlab.com/signald/signald/-/blob/main/LICENSE)


To get started, [install](/articles/install/) signald, then check out the [Getting Started](/articles/getting-started/) page.